package com.suntravels.restapi.repository;

import com.suntravels.restapi.entity.ContactNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactNumberRepository extends JpaRepository<ContactNumber, Integer> {
}
