package com.suntravels.restapi.controller;

import com.suntravels.restapi.entity.Hotel;
import com.suntravels.restapi.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HotelController {

    @Autowired
    HotelService hotelService;

    @GetMapping("/hotel")
    public List<Hotel> testRun(){
        return hotelService.hotelTest();
    }

    @PostMapping("/newHotel")
    public Hotel newHotel(@RequestBody Hotel hotel){
        return hotelService.addHotel(hotel);
    }
}
