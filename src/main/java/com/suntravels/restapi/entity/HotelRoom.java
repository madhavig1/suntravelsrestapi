package com.suntravels.restapi.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table("hotel_room")
public class HotelRoom {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private double price;
    @NotNull
    private Integer max_allowed_adults;
    @NotNull
    private Integer no_of_rooms;

    @NotNull
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="room_type_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private RoomType roomType;

    @NotNull
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="contract_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Contract contract;

}
