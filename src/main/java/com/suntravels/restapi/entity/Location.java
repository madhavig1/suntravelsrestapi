package com.suntravels.restapi.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.transaction.Transactional;

@Entity
@Transactional
public class Location {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private String district;

    public Location()
    {
    }

    public Location( Integer id, String district )
    {
        this.id = id;
        this.district = district;
    }

    @Override
    public String toString()
    {
        return "Location{" +
                       "id=" + id +
                       ", district='" + district + '\'' +
                       '}';
    }

    public String getDistrict()
    {
        return district;
    }

    public void setDistrict( String district )
    {
        this.district = district;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId( Integer id )
    {
        this.id = id;
    }
}
