package com.suntravels.restapi.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name="contact_number")
public class ContactNumber {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private String number;

    @NotNull
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="hotel_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Hotel hotel_id;
}
