package com.suntravels.restapi.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.transaction.Transactional;
import java.util.Date;

@Entity
@Table("contract")
public class Contract {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private Date valid_from;
    @NotNull
    private Date valid_until;

    @NotNull
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="location_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Location location;

    public Contract()
    {
    }

}
