package com.suntravels.restapi.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table("hotel_location")
public class HotelLocation
{
    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="hotel_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Hotel hotel;

    @NotNull
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="location_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Location location;

}
