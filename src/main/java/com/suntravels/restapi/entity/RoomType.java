package com.suntravels.restapi.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table("room_type")
public class RoomType {
    @Id
    @GeneratedValue
    private Integer id;
    @NotNull
    private String type_name;
}
