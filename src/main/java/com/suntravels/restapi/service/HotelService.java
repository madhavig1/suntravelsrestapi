package com.suntravels.restapi.service;

import com.suntravels.restapi.entity.Hotel;
import com.suntravels.restapi.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class HotelService {

    @Autowired
    private HotelRepository hotelRepository;
     public String testRun(){
         return "Test inside service...";
     }

     public List<Hotel> hotelTest(){
         return hotelRepository.findAll();
     }

     public Hotel addHotel(Hotel hotel){
         return hotelRepository.save(hotel);
     }
}
