package com.suntravels.restapi.service;

import com.suntravels.restapi.entity.ContactNumber;
import com.suntravels.restapi.repository.ContactNumberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ContactNumberService {
    @Autowired
    private ContactNumberRepository contactNumberRepository;

    public List<ContactNumber> getALlContacts(){
        return contactNumberRepository.findAll();
    }
}
